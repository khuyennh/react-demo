import styled from 'styled-components';

export const Footer = styled.footer`
  padding: 15px 5px;
  background: ${({theme}) => theme.colors.gray};
  font-size: ${({theme}) => theme.fontSize.md};
  text-align: center;

  a {
    color: ${({theme}) => theme.colors.primary};
  }
`;