import React from 'react'
import * as S from './style';

const Footer = () => {
  return (
    <S.Footer>
      <a
        href="http://huukhuyen.netlify.app/"
        rel="noopener noreferrer"
        target="_blank"
      >
        Follow me blog!
      </a>
    </S.Footer>
  );
}

export default Footer
