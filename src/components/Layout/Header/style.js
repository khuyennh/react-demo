import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import { rotate } from '../../../styles/mixin';

export const Wrapper = styled.header``;

export const LogoIcon = styled.div`
  text-align: center;

  img {
    animation: ${rotate} 2s infinite;
  }
`;

export const Logo = styled.div`
  margin: 16px;
  font-size: 96px;
  color: ${({ theme }) => theme.colors.primary};
  font-weight: 600;
  text-align: center;
`;

export const Nav = styled.nav`
  background: ${({ theme }) => theme.colors.gray};
`;

export const MenuList = styled.ul`
  display: flex;
  justify-content: center;
`;

export const MenuItem = styled.li`
  padding: 20px;
  transition: .35s;

  &:hover {
    background: ${({ theme }) => theme.colors.blue};
  }
`;

export const MenuLink = styled(NavLink)`
  position: relative;
  color: ${({theme}) => theme.colors.light};
  font-size: ${({ theme }) => theme.fontSize.lg};
  transition: all 0.5s ease-in-out;

  &:before {
    content: attr(data-item);
    width: 0;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    color: ${({theme}) => theme.colors.primary};
    overflow: hidden;
    transition: 0.35s;
  }

  &:hover,
  &.active {
    &:before {
      width: 100%;
    }
  }
`;