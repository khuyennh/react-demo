import React from 'react'

import * as S from './style'
import { Logo } from '../../../styles/image';

const Header = () => {
  return (
    <S.Wrapper>
      <S.Logo data-item="Demo React">Demo React</S.Logo>
      <S.LogoIcon>
        <img src={Logo} alt="" />
      </S.LogoIcon>
      <S.Nav>
        <S.MenuList className="menuItems">
          <S.MenuItem>
            <S.MenuLink to="/home" data-item="Home" activeClassName="active">
              Home
            </S.MenuLink>
          </S.MenuItem>
          <S.MenuItem>
            <S.MenuLink to="/about" data-item="About" activeClassName="active">
              About
            </S.MenuLink>
          </S.MenuItem>
          <S.MenuItem>
            <S.MenuLink
              to="/project"
              data-item="Projects"
              activeClassName="active"
            >
              Projects
            </S.MenuLink>
          </S.MenuItem>
          <S.MenuItem>
            <S.MenuLink to="/blog" data-item="Blog" activeClassName="active">
              Blog
            </S.MenuLink>
          </S.MenuItem>
          <S.MenuItem>
            <S.MenuLink
              to="/contact"
              data-item="Contact"
              activeClassName="active"
            >
              Contact
            </S.MenuLink>
          </S.MenuItem>
        </S.MenuList>
      </S.Nav>
    </S.Wrapper>
  );
}

export default Header
