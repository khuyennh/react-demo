import styled from 'styled-components';

export const Main = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-height: 100vh;
  `;

export const Body = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;