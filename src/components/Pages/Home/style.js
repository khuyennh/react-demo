import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 10px;
  background: ${({ theme }) => theme.colors.yellow};
  flex: 1;
`;
