import React from 'react'
import { HeadingH2 } from '../../common/style';
import * as S from './style';

const Home = () => {
  return (
    <S.Wrapper>
      <HeadingH2 center>Home Content</HeadingH2>
    </S.Wrapper>
  )
}

export default Home
