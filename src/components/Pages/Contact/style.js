import styled from 'styled-components';
import { HeadingH3 } from '../../common/style';

export const Wrapper = styled.div`
  padding: 10px;
  background: ${({ theme }) => theme.colors.blue};
  flex: 1;
`;

export const ContactWrapper = styled.div`
  max-width: 400px;
  width: 100%;
  margin: 0 auto;
  position: relative;
`;

export const Field = styled.input`
  width: 100%;
  border: 1px solid ${({ theme }) => theme.colors.light};
  margin-bottom: 5px;
  padding: 10px;
`;

export const ButtonForm = styled.button`
  width: 100%;
  padding: 10px;
  color: ${({ theme }) => theme.colors.white};
  background: ${({ theme }) => theme.colors.primary};
  border: 0;
`;

export const Form = styled.form`
  background: #f9f9f9;
  padding: 25px;
  margin: 50px 0;

  h3 {
    color: #f96;
    display: block;
    font-size: 30px;
    font-weight: 400;
  }
`;

export const TitleForm = styled(HeadingH3)`
  margin: 20px 0;
`;
