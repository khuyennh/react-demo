import React from 'react';
import { HeadingH2 } from '../../common/style';
import * as S from './style';

const Contact = () => {
  return (
    <S.Wrapper>
      <HeadingH2 center>Contact Content</HeadingH2>
      <S.ContactWrapper>
        <form id="contact" action method="post">
          <S.TitleForm>Quick Contact</S.TitleForm>
          <S.Field
            placeholder="Your name"
            type="text"
          />
          <S.Field
            placeholder="Your Email Address"
            type="email"
          />
          <S.Field
            placeholder="Your Phone Number"
            type="tel"
          />
          <S.Field
            placeholder="Your Web Site starts with http://"
            type="url"
          />
          <S.Field
            placeholder="Type your Message Here...."
          />
          <S.ButtonForm
            name="submit"
            type="submit"
            id="contact-submit"
          >
            Submit
          </S.ButtonForm>
        </form>
      </S.ContactWrapper>
    </S.Wrapper>
  );
};

export default Contact;
