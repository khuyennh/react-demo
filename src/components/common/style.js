import styled from 'styled-components';

export const HeadingH2 = styled.h2`
  color: ${({ theme }) => theme.colors.primary};
  font-size: ${({ theme }) => theme.fontSize.xl2};
  text-align: ${({center}) => center ? 'center' : ''};
`;

export const HeadingH3 = styled.h3`
  color: ${({ theme }) => theme.colors.blueGrey};
  font-size: ${({ theme }) => theme.fontSize.xl};
  text-align: ${({ center }) => center ? 'center' : ''};
`;