export { default as Logo } from './images/logo192.png';

export { default as IconLogoAdmin } from './images/icon_arrow_up.svg';
export { default as IconAvatar } from './images/icon_avatar.svg';
export { default as IconDownload } from './images/icon_download.svg';