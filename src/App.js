import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Home from './components/Pages/Home';
import Contact from './components/Pages/Contact';
import Header from './components/Layout/Header';
import Footer from './components/Layout/Footer';

import * as S from './components/Layout/style';

function App() {
  return (
    <S.Main>
      <BrowserRouter>
        <Header />
        <S.Body>
          <Route exact path="/home" component={Home} />
          <Route path="/contact" component={Contact} />
        </S.Body>
        <Footer />
      </BrowserRouter>
    </S.Main>
  );
}

export default App;
